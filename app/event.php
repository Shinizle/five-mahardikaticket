<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class event extends Model
{
    protected $table = 'event';
    protected $fillable = [
    	'name',
    	'description',
    	'location',
    	'date',
    	'start_time',
    	'end_time',
    	'image',
        'price'
    ];

    public function getTopEvent()
    {
        $query  = DB::select("SELECT a.*, COUNT(b.id) as total, b.id_event 
            FROM `event` a JOIN `ticket` b ON a.id = b.id_event 
            WHERE status = 'SOLD' GROUP BY id_event DESC  ORDER BY `total` DESC LIMIT 3"
        );
        return $query;
    }

    public function getDefaultTopEvent()
    {
        $query  = DB::table('event')->orderBy('id', 'DESC')->limit(3);
        return $query;
    }

    public function getAllEvent()
    {
        $query  = DB::table('event')->orderBy('id', 'DESC');
        return $query;
    }

    public function getAllEventAndAvailableTicket()
    {
        $query = DB::select("SELECT * FROM `event`, (SELECT id_event, COUNT(id) AS total FROM `ticket` WHERE status = 'Available' GROUP BY id_event) AS tb_ticket WHERE  event.id = tb_ticket.id_event GROUP BY name ORDER BY id DESC");
        return $query;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ticket extends Model
{
    protected $table = 'ticket';
    protected $fillable = [
    	'serialcode',
    	'id_event',
    	'status',
    	'id_user',
    ];

    public function checkAvailableTicket($id_event)
    {
    	$query 	= DB::table('ticket')
    		->where([
    			['status', '=', 'Available'],
    			['id_event', '=', $id_event]
    		]);
    	return $query;
    }

    public function updateTicketLimitOne($id_event,$data)
    {
    	DB::table('ticket')
    	->where([
    		['id_event', '=', $id_event],
    		['status', '=', 'Available']
    	])->limit(1)
    	->update($data);
    }

    public function checkDuplicateTicket($user_id,$id_event)
    {
    	$query 	= DB::table('ticket')
	    	->where([
	    		['id_event', '=', $id_event],
	    		['id_user', '=', $user_id]
	    	]);
	    return $query;
    }

    public function getUserTicket($id)
    {
    	$query 	= DB::table('ticket')
    		->join('event', 'event.id', '=', 'ticket.id_event')
    		->where('id_user', $id);
    	return $query;
    }

    public function getAllTicket($id)
    {
        $query  = DB::table('ticket')
            ->select(DB::raw('ticket.*, users.name as belongtouser'))
            ->leftjoin('users', 'users.id', '=', 'ticket.id_user')
            ->join('event', 'event.id', '=', 'ticket.id_event')
            ->where('id_event', $id);
        return $query;
    }
}

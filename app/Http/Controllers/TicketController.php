<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
Use App\Ticket;
use Auth;

class TicketController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
        $this->ticket = new Ticket;
    }

    public function generate($id, Request $request)
    {
        $this->validate($request,[
            'ticket' => 'required',
        ]);
        $counter    = $request->ticket;
        $string1    = substr($request->name,0,2);
        $string2    = substr($request->location,0,1);
        $getmin     = date('i');
        $gethour    = date('H');
        $getyear    = date('y');
        $getmonth   = date('m');
        $getsec     = date('s');
        $data       = [];
        for($i = 1; $i <= $counter; $i++) {
            $number = str_pad($i,3,"0",STR_PAD_LEFT);
            $data   = [
                'serialcode'    => $getyear.$string1.'-'.$getmonth.$gethour.'-'.$getmin.$getsec.'-'.$string2.$number,
                'id_event'      => $id,
                'status'        => 'Available',
                'id_user'       => NULL
            ];
            Ticket::insert($data);
        }
        return redirect()->back()->with(["success" => $counter.' new ticket is ready to serve!']);
    }

    public function expiringTicket($id)
    {
        $ticket         = Ticket::find($id);
        $ticket->status = 'Expired';
        $ticket->save();
        return redirect()->back()->with(['success' => $ticket->serialcode.' has been expired!']);
    }

    public function recyleTicket($id)
    {
        $ticket         = Ticket::find($id);
        $ticket->status = 'Available';
        $ticket->save();
        return redirect()->back()->with(['success' => $ticket->serialcode.' has been renewed!']);
    }

    function checkoutTicket($id_event) {
    	$ticket = $this->ticket->checkAvailableTicket($id_event);
    	if($ticket->get()->count() > 0) {
    		$dupecheck 	= $this->ticket->checkDuplicateTicket(Auth::user()->id, $id_event);
    		if($dupecheck->count() > 0) {
    			return view('info_page.info_ticket_is_ready');
    		} else {
	    		$data 	= [
	    			'id_user' 	=> Auth::user()->id,
	    			'status' 	=> 'Sold',
	    		];
	    		$this->ticket->updateTicketLimitOne($id_event,$data);
	    		return view('info_page.info_success');
	    	}
    	} else {
    		return view('info_page.info_soldout');
    	}
	}
}

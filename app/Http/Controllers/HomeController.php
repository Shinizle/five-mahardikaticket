<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Ticket;
use App\Event;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __construct()
    {
        $this->ticket   = new Ticket;
        $this->event    = new Event;
    }

    public function index()
    {
        if(Auth::guest()) {
            return redirect('/main');}
        if (Auth::user()->role_id == 1) { 
            return redirect('/dashboard');
        } elseif (Auth::user()->role_id == 2 || Auth::guest()){ 
            return redirect('/main');
        }
    }

    public function main()
    {
        $top_event = $this->event->getTopEvent();
        if(count($top_event) >= 3) {
            $best_seller = $top_event;
        } else {
            $best_seller = $this->event->getDefaultTopEvent()->get();
        }
        $data   = [
            'best_seller'   => $best_seller,
            'event'         => $this->event->getAllEvent()->paginate(5)
        ];
        return view('homepage', $data);
    }

    public function ticket($id)
    {
        $event  = Event::find($id);
        if(Auth::guest()) {
            $duplicate  = 0;
        } else {
            $duplicate  = $this->ticket->checkDuplicateTicket(Auth::user()->id, $id)->count();
        }
        $data   = [
            'event'     => $event,
            'duplicate' => $duplicate
        ];
        return view('ticket', $data);
    }

    public function profile($id)
    {
        $ticket = $this->ticket->getUserTicket($id);
        $data   = [
            'counter'   => 1,
            'ticket'    => $ticket->paginate(5)
        ];
        return view('profile', $data);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Event;
Use App\Ticket;
use Auth;

class EventController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->ticket   = new Ticket;
        $this->event    = new Event;
    }

    public function index()
    {
        $event 	= $this->event->getAllEventAndAvailableTicket();
        $data 	= [
        	'event' 	=> $event,
        	'counter' 	=> 1
        ];
    	return view('admin.dashboard', $data);
    }

    public function create()
    {
    	return view('admin.create_event');
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' 			=> 'required',
    		'description' 	=> 'required',
    		'location' 		=> 'required',
    		'date' 			=> 'required',
    		'start' 		=> 'required',
    		'endevent' 		=> 'required',
    		'image' 		=> 'required|file|image|mimes:jpeg,png,jpg|max:2048',
    		'price' 		=> 'required',
    	]);
    	
    	// processing image
 		$file 		= $request->file('image');
 		$filename 	= time()."_".$file->getClientOriginalName();
 		$imagepath 	= 'event_image';
		$file->move($imagepath,$filename);
		
		// store data to database with eloquent
        Event::create([
    		'name' 			=> $request->name,
    		'description' 	=> $request->description,
    		'location'	 	=> $request->location,
    		'date' 			=> $request->date,
    		'start_time'	=> $request->start,
    		'end_time' 		=> $request->endevent,
    		'image' 		=> $filename,
    		'price' 		=> $request->price
    	]);

        // generate 1st 10 tickets for event
        $new_event 	= Event::orderBy('id', 'DESC')->limit(1)->get();
    	$counter 	= 10;
		$string1 	= substr($new_event[0]->name,0,2);
		$string2 	= substr($new_event[0]->location,0,1);
		$id_event 	= $new_event[0]->id;
		$getmin		= date('i');
		$gethour 	= date('H');
		$getyear 	= date('y');
		$getmonth 	= date('m');
		$getsec 	= date('s');
		$data 		= [];
		for($i = 1; $i <= $counter; $i++) {
			$number = str_pad($i,3,"0",STR_PAD_LEFT);
			$data 	= [
				'serialcode' 	=> $getyear.$string1.'-'.$getmonth.$gethour.'-'.$getmin.$getsec.'-'.$string2.$number,
				'id_event' 		=> $id_event,
				'status' 		=> 'Available',
				'id_user' 		=> NULL
			];
			Ticket::insert($data);
		}
 
	    return redirect('/dashboard')->with(['success' => 'New event has been created!']);
    }

    public function edit($id)
    {
    	$event 	= Event::find($id);
    	$data 	= [
    		'event' => $event,
    	];
	   	return view('admin.edit_event', $data);
    }

    public function update($id, Request $request)
	{
	    $this->validate($request,[
		   	'name' 			=> 'required',
    		'description' 	=> 'required',
    		'location' 		=> 'required',
    		'date' 			=> 'required',
    		'start' 		=> 'required',
    		'endevent' 		=> 'required',
    		'price' 		=> 'required'
	    ]);
	 
	    $event 				= Event::find($id);
	    $event->name 		= $request->name;
	    $event->description = $request->description;
	    $event->location 	= $request->location;
	    $event->date 		= $request->date;
	    $event->start_time 	= $request->start;
	    $event->end_time 	= $request->endevent;
	    $event->price 		= $request->price;
	    $event->save();
	    return redirect('/dashboard')->with(['success' => $request->name.' has been updated!']);
	}

	public function updateImage($id, Request $request)
	{
		$this->validate($request,[
    		'image' 		=> 'required|file|image|mimes:jpeg,png,jpg|max:2048'
    	]);
 		$file 			= $request->file('image');
 		$filename 		= time()."_".$file->getClientOriginalName();
 		$imagepath 		= 'event_image';
		$file->move($imagepath,$filename);
		$event 			= Event::find($id);
		$event->image 	= $filename;
		$event->save();
	    return redirect()->back()->with(['success' => 'Image file has been updated!']);;
	}

	public function delete($id)
	{
		$event 	= Event::find($id);
	    $event->delete();
	    return redirect()->back()->with(['success' => $event->name.' has been deleted!']);;
	}

	public function detail($id)
	{
		$event 	= Event::find($id);
		$ticket = $this->ticket->getAllTicket($id)->get();
        $data 	= [
        	'event' 	=> $event,
        	'ticket' 	=> $ticket,
        	'counter' 	=> 1
        ];
    	return view('admin.detail_event', $data);
	}

}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
        	'name' 		=> 'Irvan Kharisma Fihan',
        	'username' 	=> 'administrator',
        	'email' 	=> 'irvankharismafihan@dev.com',
        	'password' 	=> bcrypt('login123'),
        	'role_id' 	=> 1
        ]);

        App\User::create([
        	'name' 		=> 'John Doe',
        	'username' 	=> 'john',
        	'email' 	=> 'john@dev.com',
        	'password' 	=> bcrypt('login123'),
        	'role_id' 	=> 1
        ]);
    }
}

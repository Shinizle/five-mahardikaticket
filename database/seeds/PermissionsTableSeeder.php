<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Permission::create([
        	'name' 	=> 'view data' 
        ]);
        App\Permission::create([
        	'name' 	=> 'create data'
        ]);
        App\Permission::create([
        	'name' 	=> 'edit data'
        ]);
        App\Permission::create([
        	'name' 	=> 'update data'
        ]);
        App\Permission::create([
        	'name' 	=> 'delete data'
        ]);

        $admin 		= App\Role::where('name', 'admin')->first();
        $admin->permissions()->attach([1, 2, 3, 4, 5]);

        $customer 	= App\Role::where('name', 'customer')->first();
        $customer->permissions()->attach([1, 2]);

    }
}

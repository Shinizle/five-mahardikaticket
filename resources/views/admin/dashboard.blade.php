@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if ($message = Session::get('success'))
          <div class="alert alert-info alert-dismissible">
              <button type="button" class="close text-white" data-dismiss="alert" aria-hidden="true">×</button>
              <h5><i class="icon fas fa-info"></i> Info!</h5>
              {{ $message }}
          </div>
        @endif

        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="card-title"><b>EVENT LIST</b></h3>
              </div>
              <div class="col-sm-6">
                <a class="btn btn-primary float-right text-white" href="/event/create">Add Data</a>
              </div>
            </div>
          <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th class="text-center">Event</th>
                    <th>Location</th>
                    <th>Date</th>
                    <th>Ticket</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($event as $get)
                    <tr>
                      <td>{{ $counter++ }}</td>
                      <td class="text-center">
                        <img class="img-fluid pad" src="/event_image/{{ $get->image }}" alt="Photo"><br>
                        <span class="username"><a href="/event/detail/{{ $get->id }}">{{ $get->name }}</a></span>
                      </td>
                      <td>{{ $get->location }}</td>
                      <td>{{ date('l, Y-m-d', strtotime($get->date)) }}</td>
                      <td>{{ $get->total }} Tickets Available</td>
                      <td width="200px" class="text-center">
                        <a href="/event/detail/{{ $get->id }}" class="btn btn-primary btn-sm text-white">Detail</a>
                        <a href="/event/edit/{{ $get->id }}" class="btn btn-success btn-sm text-white">Edit</a>
                        <a href="/event/delete/{{ $get->id }}" class="btn btn-danger btn-sm text-white">Delete</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>NO</th>
                    <th class="text-center">Event</th>
                    <th>Location</th>
                    <th>Date</th>
                    <th>Ticket</th>
                    <th>Options</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
    </section>
    <!-- /.content -->

    @endsection
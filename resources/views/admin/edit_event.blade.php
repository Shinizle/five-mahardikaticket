@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit {{ $event->name }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="#">Edit</a></li>
              <li class="breadcrumb-item active">{{ $event->name }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <div class="row">
          <div class="col-sm-6">
            @if ($message = Session::get('success'))
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-info"></i> Info!</h5>
                {{ $message }}
              </div>
            @endif
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Event Form</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/event/update/{{ $event->id }}" enctype="multipart/form-data">

                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Event Name</label>
                    <input type="text" name="name" value="{{ $event->name }}" class="form-control datepicker" placeholder="Enter name">
                      @if($errors->has('name'))
                          <div class="text-danger">
                              {{ $errors->first('name')}}
                          </div>
                      @endif
                  </div>
                  <div class="form-group">
                      <label>Description</label>
                      <textarea name="description" class="form-control" rows="3" placeholder="Enter description...">{{ $event->description }}</textarea>
                      @if($errors->has('description'))
                          <div class="text-danger">
                              {{ $errors->first('description')}}
                          </div>
                      @endif
                    </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Location</label>
                    <input name="location" value="{{ $event->location }}" type="text" class="form-control" placeholder="Enter location">
                    @if($errors->has('location'))
                        <div class="text-danger">
                            {{ $errors->first('location')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Date</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input name="date" id="datepicker" value="{{ $event->date }}" type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                    </div>
                    @if($errors->has('date'))
                        <div class="text-danger">
                            {{ $errors->first('date')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Event Start At</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                      </div>
                      <input name="start" value="{{ $event->start_time }}"type="text" class="form-control" placeholder="HH:MM">
                    </div>
                    @if($errors->has('start'))
                        <div class="text-danger">
                            {{ $errors->first('start')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Event End At</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-clock" aria-hidden="true"></i></span>
                      </div>
                      <input name="endevent" value="{{ $event->end_time }}" type="text" class="form-control" placeholder="HH:MM">
                    </div>
                    @if($errors->has('endevent'))
                        <div class="text-danger">
                            {{ $errors->first('endevent')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Price</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="" aria-hidden="true">Rp.</i></span>
                      </div>
                      <input name="price" value="{{ $event->price }}" type="text" class="form-control" placeholder="Enter price">
                    </div>
                    @if($errors->has('price'))
                        <div class="text-danger">
                            {{ $errors->first('price')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Event Picture</label>
                    <div class="input-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <img class="img-fluid" src="/event_image/{{ $event->image }}" alt="Photo"><br>
                        </div>
                        <div class="col-sm-6">
                          <a class="btn btn- btn-success text-white" data-toggle="modal" data-target="#change-image">Change Picture</a>
                        </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <div class="row">
                    <div class="col-sm-12">
                      <input type="submit" class="btn btn-primary" value="Submit">
                      <a href="/dashboard" class="btn btn-danger text-white float-sm-right">Back</a>
                    </div>
                  </div>
                </div>

              </form>

            </div>

          </div>
          <div class="col-sm-6">
          </div>
          <div class="modal fade" id="change-image">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Change Image for {{ $event->name }}</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                
                <form method="POST" action="/event/updateimage/{{ $event->id }}" enctype="multipart/form-data">
                  <div class="modal-body">

                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                      <label for="exampleInputFile">Event Picture</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input name="image" type="file" class="custom-file-input" id="exampleInputFile">
                          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                      </div>
                      @if($errors->has('image'))
                          <div class="text-danger">
                              {{ $errors->first('image')}}
                          </div>
                      @endif
                    </div>

                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" value="Save changes">
                  </div>
                </form>

              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
        </div>        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    @endsection
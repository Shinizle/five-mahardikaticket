@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">New Data</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item active">Create Event</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Event Form</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="POST" action="/event/store" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Event Name</label>
                    <input type="text" name="name" class="form-control datepicker" placeholder="Enter name">
                      @if($errors->has('name'))
                          <div class="text-danger">
                              {{ $errors->first('name')}}
                          </div>
                      @endif
                  </div>
                  <div class="form-group">
                      <label>Description</label>
                      <textarea name="description" class="form-control" rows="3" placeholder="Enter description..."></textarea>
                      @if($errors->has('description'))
                          <div class="text-danger">
                              {{ $errors->first('description')}}
                          </div>
                      @endif
                    </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Location</label>
                    <input name="location" type="text" class="form-control" placeholder="Enter location">
                    @if($errors->has('location'))
                        <div class="text-danger">
                            {{ $errors->first('location')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Date</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input name="date" id="datepicker" type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask>
                    </div>
                    @if($errors->has('date'))
                        <div class="text-danger">
                            {{ $errors->first('date')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Event Start At</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                      </div>
                      <input name="start" id="timepicker-24-hr" type="text" class="form-control" placeholder="HH:MM">
                    </div>
                    @if($errors->has('start'))
                        <div class="text-danger">
                            {{ $errors->first('start')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Event End At</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-clock" aria-hidden="true"></i></span>
                      </div>
                      <input name="endevent" type="text" class="form-control" placeholder="HH:MM">
                    </div>
                    @if($errors->has('endevent'))
                        <div class="text-danger">
                            {{ $errors->first('endevent')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Price</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="">Rp.</i></span>
                      </div>
                      <input name="price" type="text" class="form-control" placeholder="Enter price..">
                    </div>
                    @if($errors->has('price'))
                        <div class="text-danger">
                            {{ $errors->first('price')}}
                        </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">Event Picture</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="image" type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                    @if($errors->has('image'))
                        <div class="text-danger">
                            {{ $errors->first('image')}}
                        </div>
                    @endif
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <div class="row">
                    <div class="col-sm-12">
                      <input type="submit" class="btn btn-primary" value="Submit">
                      <a href="/dashboard" class="btn btn-danger text-white float-sm-right">Back</a>
                    </div>
                  </div>
                </div>

              </form>

            </div>

          </div>
          <div class="col-sm-6">
          </div>
        </div>        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    @endsection
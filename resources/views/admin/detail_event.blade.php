@extends('layouts.admin')

@section('content')
<!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ticket Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
              <li class="breadcrumb-item"><a href="#">Detail</a></li>
              <li class="breadcrumb-item active">{{ $event->name }}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        @if ($message = Session::get('success'))
          <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Info!</h5>
            {{ $message }}
          </div>
        @endif

        <div class="card card-widget">
          <div class="card-header">
            <div class="user-block">
              <img class="img-circle" src="{{ asset('Assets/Logo.PNG') }}" alt="User Image">
              <span class="username"><a href="#">{{ $event->name }}</a></span>
              <span class="description">Shared publicly - {{ $event->created_at }}</span>
            </div>
            <!-- /.user-block -->
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                <i class="far fa-circle"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
              </button>
            </div>
            <!-- /.card-tools -->
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4 text-center">
                <img class="img-fluid pad" src="/event_image/{{ $event->image }}" alt="Photo">
                <h5 class="text-danger"><b>Live at {{ $event->location }}, {{ $event->date }}</b></h5>
                <p>{{ $event->start_time }} to {{ $event->end_time }}</p> 
              </div>
              <div class="col-sm-8">
                <h2>Description</h2>
                <p> {{ $event->description }} </p>
              </div>
            </div>

          </div>
          <div class="card-footer">
            <a href="/dashboard" class="btn btn-success text-white">Rp. {{ number_format($event->price) }},-</a>
            <a href="/dashboard" class="btn btn-warning float-right text-white">Back</a>
          </div>
          <!-- /.card-footer -->
        </div>

        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-sm-6">
                <h3 class="card-title"><b>Tickets for {{ $event->name }}</b></h3>
              </div>
              <div class="col-sm-6">
                <a class="btn btn-primary float-right text-white" data-toggle="modal" data-target="#generate">Generate Ticket</a>
              </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>NO</th>
                  <th>Serialcode</th>
                  <th>Status</th>
                  <th>Belong To</th>
                  <th>Options</th>
                </tr>
              </thead>
              <tbody>
                @foreach($ticket as $get)
                  <tr>
                    <td>{{ $counter++ }}</td>
                    <td>{{ $get->serialcode }}</td>
                    <td class="text-primary">{{ $get->status }}</td>
                    <td>{{ $get->belongtouser }}</td>
                    <td width="100px" class="text-center">
                      @if ($get->status === 'Expired')
                        <a href="/event/ticket/recycle/{{ $get->id }}" class="btn btn-warning btn-sm text-white">Recyle</a>
                      @elseif ($get->status === 'Available')
                        <a href="/event/ticket/expire/{{ $get->id }}" class="btn btn-primary btn-sm text-white">Expire</a>
                      @elseif ($get->status === 'Sold')
                        <a class="btn-sm text-primary">No Action</a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Serialcode</th>
                  <th>Status</th>
                  <th>Belong To</th>
                  <th>Options</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>

        <div class="modal fade" id="generate">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Generate Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              
              <form method="POST" action="/event/ticket/generate/{{ $event->id }}">
                <div class="modal-body">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}
                  
                  <div class="form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-sync-alt"></i></span>
                      </div>
                      <input name="ticket" type="number" class="form-control" min='1' max='100'>
                    </div>
                    @if($errors->has('date'))
                        <div class="text-danger">
                            {{ $errors->first('date')}}
                        </div>
                    @endif
                  </div>

                  <p class="text-danger">
                    **Insert number of ticket you want to generate.<br>
                  </p>
                  <input type="hidden" name="name" value="{{ $event->name }}">
                  <input type="hidden" name="location" value="{{ $event->location }}">
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-primary" value="Generate">
                </div>
              </form>

            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    @endsection
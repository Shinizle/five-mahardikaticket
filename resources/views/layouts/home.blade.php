<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Mahardika Ticket</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('../../plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('../../dist/css/adminlte.min.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style type="text/css">
    .jumbotron {
      background-image: url("{{ asset('Assets/head1.png')}}");
      background-size: cover;
    }
    .img none { border:none!important; }
  </style>
</head>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="/home" class="navbar-brand">
        <img src="{{ asset('Assets/Logo.PNG') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Mahardika Ticket</span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="/main" class="nav-link">Home</a>
          </li>
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="#" class="nav-link">About Us</a>
          </li>
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="#" class="nav-link">Contact</a>
          </li>

        </ul>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <div class="collapse navbar-collapse order-3" id="navbarCollapse"> 
          @if (Route::has('login'))
            @auth
              <li class="nav-item">
                <a href="/profile/{{ Auth::user()->id }}" class="nav-link">Profile</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </li>
            @else

            <li class="nav-item">
                <a href="{{ route('login') }}" class="nav-link">Login</a>
            </li>

                @if (Route::has('register'))
                    <li class="nav-item">
                <a href="{{ route('register') }}" class="nav-link">Register</a>
            </li>
                @endif
            @endauth
          @endif
      </ul>

    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer" style="height: 120px">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      <br>
      <p>
        © PT. Mahardika Solusi Teknologi, 2020.<br>
        <a class="float-right">We love our users!</a>
      </p>
    </div>
    <!-- Default to the left -->
      <br>
      <a href="/home" class="navbar-brand">
        <img src="{{ asset('Assets/Logo.PNG') }}" alt="AdminLTE Logo" class="img-circle"
             style="opacity: .8; width: 50px;">
        <span class="brand-text font-weight-light">Mahardika Ticket</span>
      </a>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="[[ asset('../../plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="[[ asset('../../plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="[[ asset('../../dist/js/adminlte.min.js') }}"></script>
</body>
</html>

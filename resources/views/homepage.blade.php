@extends('layouts.home')

@section('content')

  <div class="jumbotron text-center">
    <h1 class="display-4"><b>Best Seller!</b></h1><br>
    <p>
      <div class="row">
        @foreach($best_seller as $get)
          <div class="col-sm-4">
            <a href="/event/ticket/{{$get->id}}">
              <img width='100%' class="img-fluid pad" src="/event_image/{{$get->image}}" alt="Photo">
            </a>
          </div>
        @endforeach
      </div>
    </p>
  </div>

  <div class="container-fluid">       
  
    @foreach($event as $get)
      <div class="card flex-row flex-wrap">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <a href="/event/checkout/{{ $get->id }}">
                  <img width='100%'src="/event_image/{{$get->image}}" class="img" alt="">
                </a>
              </div>
              <div class="col-sm-8">
                <div class="card-block px-2">
                  <div class="row">
                    <div class="col-sm-8">
                      <a href="/event/ticket/{{ $get->id }}">
                        <h2 class="text-primary"><b>{{ $get->name }}</b></h2>
                      </a>
                    </div>
                    <div class="col-sm-4">
                      <h2 class="float-right">Rp. {{ number_format($get->price) }},-</h2>
                    </div>
                  </div><br>
                    <h4 class="card-text">{{ date('l, d-m-Y', strtotime($get->date)) }}</h4>
                    <h5 href="/event/ticket/{{ $get->id }}" class="text-danger"><b>19:00 - 23:00</b></h5>
                    <a href="/event/ticket/{{ $get->id }}" class="btn btn-success btn-lg btn-flat float-right text-white">
                      <i class="fas fa-cart-plus fa-lg mr-2"></i> 
                      BUY NOW &nbsp;
                    </a>
                </div>
              </div>
            </div>
          </div>
      </div>
    @endforeach
    <div class="d-flex justify-content-center">
        {!! $event->links() !!}
    </div>
  </div>

@endsection
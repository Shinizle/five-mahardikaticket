@extends('layouts.home')

@section('content')
<section class="content">
  <div><br></div>
  <div class="container text-center">
    <h2 class="headline text-success"> SUCCESS!</h2>
      <h3><i class="fas fa-exclamation-triangle text-success"></i> Ticket has been seen tou your account!</h3>

      <p>
        Thank you, please enjoy your show! <a href="/main">return to dashboard</a>
      </p>

  </div>
</section>
@endsection
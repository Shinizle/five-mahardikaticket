@extends('layouts.home')

@section('content')
<section class="content">
  <div><br></div>
  <div class="container text-center">
    <h2 class="headline text-warning"> DUPLICATED</h2>
    <h3><i class="fas fa-exclamation-triangle text-warning"></i> You have own this ticket, please check your account.</h3>

    <p>
      Thank you, please enjoy your show! <a href="/main">return to dashboard</a>
    </p>
  </div>
</section>
@endsection
@extends('layouts.home')

@section('content')
<section class="content">
  <div><br></div>
  <div class="container text-center">
    <h2 class="headline text-warning"> SOLD OUT</h2>
      <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! No available ticket left.</h3>

      <p>
        We could not find any available ticket left, please comeback later! <a href="/main">return to dashboard</a>
      </p>
  </div>
</section>
@endsection
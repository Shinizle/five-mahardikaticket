@extends('layouts.home')

@section('content')

  <div class="container"> <br>      
    <h2><p>Your Ticket</p></h2>
    @foreach($ticket as $get)
      <div class="card flex-row flex-wrap">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <a href="/event/checkout/{{ $get->id }}">
                  <img width='100%' src="/event_image/{{$get->image}}" class="img" alt="">
                </a>
              </div>
              <div class="col-sm-8">
                <div class="card-block px-2">
                  <div class="row">
                    <div class="col-sm-8">
                      <a href="/event/ticket/{{ $get->id }}">
                        <h2 class="text-primary"><b>{{ $get->name }}</b></h2>
                      </a>
                    </div>
                  </div><br>
                    <h4 class="card-text">{{ date('l, d-m-Y', strtotime($get->date)) }}</h4>
                    <h5 href="/event/ticket/{{ $get->id }}" class="text-danger"><b>19:00 - 23:00</b></h5>
                    <a class="btn btn-success btn-lg btn-block float-right text-white">
                      {{ $get->serialcode }}
                    </a>
                </div>
              </div>
            </div>
          </div>
      </div>
    @endforeach
    <div class="d-flex justify-content-center">
        {!! $ticket->links() !!}
    </div>
  </div>

@endsection
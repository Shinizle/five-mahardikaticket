@extends('layouts.home')

@section('content')

<section class="content">
	<div><br></div>
  	<div class="container-fluid">       
	    <div class="card card-widget">
            <img class="img-fluid pad" style="width: 100%" src="/event_image/{{ $event->image }}" alt="Photo">
	      	<!-- /.card-header -->
	      	<div class="card-body">
            	<h1 class="text-info" style="font-size: 300%"><b> {{ $event->name }}</b></h1>
	        	<div class="row">
	          		<div class="col-sm-4">
	            		<h5 class="offset-md-1">
	            			 {{ date('l, Y-m-d', strtotime($event->date)) }}
	            		</h5>
	            		<h2 class="text-danger offset-md-1">
	            			{{ date('H:i', strtotime($event->start_time)) }} - 
	            			{{ date('H:i', strtotime($event->end_time)) }}
	            		</h2> 
	          		</div>
		          	<div class="col-sm-8">
		            	<h1 class="float-right"> Rp. {{ number_format($event->price) }},- </h1>
		          	</div>
	        	</div><br>
	        	<p><b> {{ $event->description }} </b></p>
	      	</div>
	      	<div class="card-footer">
	      		<div class="row"> 
	      			<div class="col-sm-2 offset-sm-10">
		        	@if ($duplicate > 0)
		        		<button class="btn btn-success btn-block text-white" disabled>
		                  	<i class="fas fa-check"></i>
		              		OWNED
		              	</button>
		        	@else
		                <a href="/event/ticket/checkout/{{ $event->id }}" class="btn btn-success btn-block text-white">
		                  Booking Ticket 
		                </a>
		            @endif
		        </div>
	      	</div>
	      	<!-- /.card-footer -->
	    </div>
  	</div>
</section>

@endsection
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/main');
});

Auth::routes();
// General Routes
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/main', 'HomeController@main');
Route::get('/profile/{id}', 'HomeController@profile');
Route::get('/event/ticket/{id}', 'HomeController@ticket');

// Transaction
Route::get('/event/ticket/checkout/{id}', 'TicketController@checkoutTicket');
Route::get('/event/ticket/checkout/result', 'EventController@resultPage');

// Admin Routes
Route::get('/dashboard', 'EventController@index');
Route::get('/event/create', 'EventController@create');
Route::get('/event/edit/{id}', 'EventController@edit');
Route::get('/event/delete/{id}', 'EventController@delete');
Route::get('/event/detail/{id}', 'EventController@detail');
Route::get('/event/ticket/expire/{id}', 'TicketController@expiringTicket');
Route::get('/event/ticket/recycle/{id}', 'TicketController@recyleTicket');
// store data
Route::post('/event/store', 'EventController@store');
// change data
Route::put('/event/update/{id}', 'EventController@update');
Route::put('/event/updateimage/{id}', 'EventController@updateImage');
Route::put('/event/ticket/generate/{id}', 'TicketController@generate');
